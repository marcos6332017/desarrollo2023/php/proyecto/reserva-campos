<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Socios $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="socios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellidos')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'talefono')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fechahora')->input('datetime-local') ?>
    <?= $form->field($model, 'username')->textInput() ?>
    <?= $form->field($model, 'password')->input('password') ?>
    <?= $form->field($model, 'administrador')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>