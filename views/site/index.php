<?php

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent mt-5 mb-5">
        <h1 class="display-4">Bienvenido a la Pagina de resarva de campos de Cantabria</h1>

        <p class="lead">Disfruta de tu tiempo libre con tus amigos en los campos</p>

        <p><a class="btn btn-lg btn-success" href="http://localhost/desarrollo2023/yii/ejemplos/miaplicacion/web/socios/create">Hazte Socio Aqui!!</a></p>
        <p><a class="btn btn-lg btn-success" href="http://localhost/desarrollo2023/yii/ejemplos/miaplicacion/web/alquileres/create">RESERVA AQUI!!</a></p>
    </div>
    <div class="body-content">
    </div>
</div>