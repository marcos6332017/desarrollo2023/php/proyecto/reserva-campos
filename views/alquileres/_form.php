<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Alquileres $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="alquileres-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'idSocio')->textInput() ?>

    <?= $form->field($model, 'idCampo')->textInput() ?>

    <?= $form->field($model, 'fechaHora')->input('datetime-local') ?>

    <?= $form->field($model, 'horas')->textInput() ?>

    <?= $form->field($model, 'personas')->textInput() ?>

    <?= $form->field($model, 'precioTotal')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>