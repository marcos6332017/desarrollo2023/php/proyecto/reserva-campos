<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class Socios extends ActiveRecord  implements \yii\web\IdentityInterface
{
    public $password_repeat; // propiedad para que cuando creo el usuario repita la contraseña
    public $codigo; // es para el captcha

    public static function tableName()
    {
        return 'socios';
    }


    public function rules()
    {
        return [
            [['id', 'administrador'], 'safe'],
            [['nombre', 'password', 'apellidos'], 'string', 'max' => 255],
            [['nombre', 'password', 'email', 'username'], 'required', 'message' => 'El campo {attribute} es obligatorio'],
            // que el usuario no exista
            [['username', 'email'], 'unique', 'message' => 'El {attribute} ya existe en el sistema'],
            ['password', 'string', 'min' => 4, 'message' => 'la contraseña debe tener al menos 6 caracteres'],
            ['email', 'email', 'message' => 'Escribe un correo correctamente'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'password' => 'Contraseña',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /* public function validatePassword($password)
      {
      return Yii::$app->security->validatePassword($password, $this->password_hash);
      } */

    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return null;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    public function beforeSave($insert)
    {

        if (parent::beforeSave($insert)) {

            if ($this->isNewRecord) {

                $this->password = Yii::$app->security->generatePasswordHash($this->password);
            } else {
                if (!empty($this->getDirtyAttributes(["password"]))) {
                    $this->password = Yii::$app->security->generatePasswordHash($this->password);
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Comprobamos si el usuario es administrador
     * @param mixed $id
     * @return bool
     */
    public static function isUserAdmin($id)
    {
        if (User::findOne(['id' => $id, 'administrador' => true])) {
            return true;
        } else {

            return false;
        }
    }

    /**
     * Summary of isUserSimple
     * @param mixed $id
     * @return bool
     */
    public static function isUserSimple($id)
    {
        if (User::findOne(['id' => $id, 'administrador' => false])) {
            return true;
        } else {

            return false;
        }
    }
}
