<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alquileres".
 *
 * @property int $id
 * @property int|null $idSocio
 * @property int|null $idCampo
 * @property string|null $fechaHora
 * @property int|null $horas
 * @property int|null $personas
 * @property float|null $precioTotal
 *
 * @property Campos $idCampo0
 * @property Socios $idSocio0
 */
class Alquileres extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alquileres';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'idSocio', 'idCampo', 'horas', 'personas'], 'integer'],
            [['fechaHora'], 'safe'],
            [['precioTotal'], 'number'],
            [['idSocio', 'idCampo', 'fechaHora'], 'unique', 'targetAttribute' => ['idSocio', 'idCampo', 'fechaHora']],
            [['id'], 'unique'],
            [['idCampo'], 'exist', 'skipOnError' => true, 'targetClass' => Campos::class, 'targetAttribute' => ['idCampo' => 'id']],
            [['idSocio'], 'exist', 'skipOnError' => true, 'targetClass' => Socios::class, 'targetAttribute' => ['idSocio' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idSocio' => 'Id Socio',
            'idCampo' => 'Id Campo',
            'fechaHora' => 'Fecha Hora',
            'horas' => 'Horas',
            'personas' => 'Personas',
            'precioTotal' => 'Precio Total',
        ];
    }

    /**
     * Gets query for [[IdCampo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdCampo0()
    {
        return $this->hasOne(Campos::class, ['id' => 'idCampo']);
    }

    /**
     * Gets query for [[IdSocio0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdSocio0()
    {
        return $this->hasOne(Socios::class, ['id' => 'idSocio']);
    }
}
