<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "campos".
 *
 * @property int $id
 * @property string|null $nombre
 * @property int|null $aforo
 * @property float|null $precio
 * @property string|null $direccion
 * @property string|null $telefono
 * @property string|null $tipo
 *
 * @property Alquileres[] $alquileres
 */
class Campos extends \yii\db\ActiveRecord
{
    public $archivo;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'campos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'aforo'], 'integer'],
            [['precio'], 'number'],
            [['nombre'], 'string', 'max' => 300],
            [['direccion'], 'string', 'max' => 400],
            [['telefono'], 'string', 'max' => 20],
            [['tipo'], 'string', 'max' => 200],
            [['id'], 'unique'],
            [
                ['archivo'], 'file',
                'skipOnEmpty' => true, // no es obligatorio seleccionas una imagen
                'extensions' => 'png,jpg' // extensiones permitidas
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'aforo' => 'Aforo',
            'precio' => 'Precio',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'tipo' => 'Tipo',
        ];
    }

    /**
     * Gets query for [[Alquileres]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlquileres()
    {
        return $this->hasMany(Alquileres::class, ['idCampo' => 'id']);
    }
    public function beforeValidate()
    {
        // si he seleccionado un archivo en el formulario
        if (isset($this->archivo)) {
            $this->archivo = \yii\web\UploadedFile::getInstance($this, 'archivo');
        }

        return true;
    }

    /**
     * despues de validar subo el archivo
     * 
     */
    public function afterValidate()
    {
        // compruebo si he seleccionado un archivo en el formulario
        if (isset($this->archivo)) {
            $this->subirArchivo();
            $this->foto = $this->id . $this->archivo->name;
        }

        return true;
    }
    public function subirArchivo(): bool
    {
        $this->archivo->saveAs('imgs/campos/' . $this->id . $this->archivo->name);
        return true;
    }

    /**
     * metodo que se ejecuta despues de guardar el registro 
     * en la bbdd
     * 
     * @param mixed $insert este argumento es true si estas insertando un registro y false si es una actualizacion
     * @param array $atributosAnteriores tengo todos los datos de la tabla antes de actualizar
     * @return void
     */
    public function afterSave($insert, $atributosAnteriores)
    {
        // pregunto si es una actualizacion
        if (!$insert) {
            // pregunto si he seleccionado un archivo en el formulario
            if (isset($this->archivo)) {
                // compruebo si existia foto anteriormente en la noticia
                if (isset($atributosAnteriores["foto"]) && $atributosAnteriores["foto"] != "") {
                    // elimino la imagen vieja del servidor
                    unlink('imgs/campos/' . $atributosAnteriores["foto"]);
                }
            }
        }
    }

    public function afterDelete()
    {
        // elimino la imagen de la noticia
        // cuando borro la noticia
        if (isset($this->foto) && file_exists(Yii::getAlias('@webroot') . '/imgs/campos/' . $this->foto)) {
            unlink('imgs/campos/' . $this->foto);
        }
        return true;
    }
}
